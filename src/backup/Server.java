package backup;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.prefs.Preferences;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import backup.datasource.Backup;
import backup.datasource.DAO;
import backup.datasource.HibernateHelper;
import backup.listeners.BackupIntervalService;

@Path("backup")
public class Server {

	protected final Logger logger = LogManager.getLogger(this.getClass());
	public static final String BACKUP_PATH = "C:"+File.separator+"Backup"+File.separator;
	public static final String DB_NAME = "lfs_db";
	protected DAO<Backup> dao = new DAO<>(Backup.class);
	public static Preferences PREFS = Preferences.userRoot().node(Server.class.getName());
	

	@POST @Path("/create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(@QueryParam("backupName") String backupName) throws Exception {
		Backup backup = Backup.newBackup(backupName);
		String cmd = "mysqldump --opt"
				+ " -u" + HibernateHelper.USERNAME 
				+ " -p" + HibernateHelper.PASSWORD 
				+ " --add-drop-database -B "+DB_NAME 
				+ " -r " + BACKUP_PATH + backup.getFileName();
		Process proc = Runtime.getRuntime().exec(cmd);
		int code = proc.waitFor();
		if(code == 0){
			dao.save(backup);
			return Response.ok(backup).build();
		}
		return Response.status(500).entity(getResponce(proc)).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/restore")
	public Response restore(Backup backup) throws Exception {
		String cmd = "mysql --user=" + HibernateHelper.USERNAME 
				+ " --password=" + HibernateHelper.PASSWORD 
				+ " -D" + DB_NAME 
				+ " -e\"" + "source " 
				+ BACKUP_PATH + backup.getFileName() + "\"";
		Process proc = Runtime.getRuntime().exec(cmd);
		int code = proc.waitFor();
		if(code == 0){
			create("Before Backup Restore");
			return Response.ok().build();
		}
		else{
			return Response.status(500).entity(getResponce(proc)).build();
		}
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON) 
	@Produces(MediaType.APPLICATION_JSON)
	public Backup update(Backup object) throws Exception  {
		return dao.save(object);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Backup> all() {
		return dao.all();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public boolean delete(@PathParam("id") int id) {
		Backup backup = dao.get(id);
		try{
			dao.delete(backup);
			String fileName = BACKUP_PATH+backup.getFileName();
			Files.delete(Paths.get(fileName));
			return true;
		}
		catch(Exception ex){
			ex.printStackTrace();
			logger.error(ex);
		}
		return false;
	}


	@POST
	@Consumes(MediaType.APPLICATION_JSON) 
	@Path("/set-interval")
	public Response setBackupInterval(Interval interval, 
			@QueryParam("time")String strTime){
		PREFS.putInt("interval", interval.ordinal());
		PREFS.put("backupTime", strTime);
		LocalTime time = LocalTime.parse(strTime);
		BackupIntervalSchedular.schedulExecution(interval, time);
		return Response.ok().build();
	}
	
	@POST
	@Path("/disable-interval")
	public Response disableBackupInterval(){
		BackupIntervalSchedular.shutdownNow();
		return Response.ok().build();
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("auto-backup-status")
	public AutoBackupStatus getAutoBackupStatus(){
		AutoBackupStatus status = new AutoBackupStatus();
		status.setAutoBackupOn(BackupIntervalSchedular.isRunning());
		status.setInterval(BackupIntervalService.getInterval());
		status.setBackupTime(BackupIntervalService.getBackupTime());
		return status;
	}
	
	private String getResponce(Process proc) {
		StringBuffer output = new StringBuffer();
		try {
			BufferedReader stdInput = new BufferedReader(new 
					InputStreamReader(proc.getInputStream()));
			BufferedReader stdError = new BufferedReader(new 
					InputStreamReader(proc.getErrorStream()));
			String s = null;
			while ((s = stdInput.readLine()) != null) {
				output.append(s);
			}
			while ((s = stdError.readLine()) != null) {
				if(!s.contains("[Warning]")){
					output.append(s);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output.toString().replace("mysqldump:", "").trim();

	}
}
