package backup.datasource;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.Session;
import org.hibernate.query.Query;

@Entity @Table(name="backups")
@XmlRootElement
public class Backup {

	@Id @GeneratedValue
	private Integer id;
	private String backupName;
	private String fileName;
	private LocalDateTime createdAt;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBackupName() {
		return backupName;
	}
	public void setBackupName(String name) {
		this.backupName = name;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCreatedAt() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");
		return formatter.format(createdAt);
	}
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	@Override
	public String toString() {
		return backupName;
	}
	
	public static Backup newBackup(){
		return newBackup(null);
	}
	
	public static Backup newBackup(String backupName) {
		String fileName = new Date().getTime()+".sql";
		Backup backup = new Backup();
		backup.createdAt = LocalDateTime.now();
		backup.fileName = fileName;
		if(backupName == null || backupName.trim().isEmpty()){
			long id = getLastId();
			backupName = "Backup "+ id;
		}
		backup.backupName = backupName;
		
		return backup;
	}
	
	private static int getLastId() {
		int id = 0;
		try(Session session = HibernateHelper.getSession()){
			Query<Backup> query = session.createQuery(
					"from Backup order by createdAt desc", 
					Backup.class);
			List<Backup> list = query.getResultList();
			if(!list.isEmpty()){
				id = list.get(0).getId();
			}
		}
		return id + 1;
	}
}
