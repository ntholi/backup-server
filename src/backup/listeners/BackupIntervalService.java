package backup.listeners;

import java.time.LocalTime;
import java.util.prefs.Preferences;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import backup.BackupIntervalSchedular;
import backup.Interval;
import backup.Server;

@WebListener
public class BackupIntervalService implements ServletContextListener {
	
    public void contextInitialized(ServletContextEvent sce)  {
    	String time = getBackupTime();
    	int interval = getInterval();
    	BackupIntervalSchedular.schedulExecution(Interval.of(interval),
    			LocalTime.parse(time));
    }
    
    public void contextDestroyed(ServletContextEvent sce)  { 
    	BackupIntervalSchedular.shutdownNow();
    }
    
    public static int getInterval(){
    	return Server.PREFS.getInt("interval", Interval.WEEKLY.ordinal());
    }
    
    public static String getBackupTime(){
    	return Server.PREFS.get("backupTime", LocalTime.of(18,0,0).toString());
    }
}
