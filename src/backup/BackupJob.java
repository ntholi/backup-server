package backup;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import backup.datasource.Backup;
import backup.datasource.DAO;
import backup.datasource.HibernateHelper;
import backup.listeners.BackupIntervalService;

public class BackupJob implements Runnable {

	private DAO<Backup> dao = new DAO<>(Backup.class);

	@Override
	public void run() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		String name = "Auto Backup ("+ formatter.format(LocalDateTime.now()) +")";

		try{
			Backup backup = Backup.newBackup(name);
			dao.save(backup);
			
			String cmd = "mysqldump --opt"
					+ " -u" + HibernateHelper.USERNAME 
					+ " -p" + HibernateHelper.PASSWORD 
					+ " --add-drop-database -B "+Server.DB_NAME 
					+ " -r " + Server.BACKUP_PATH + backup.getFileName();
			Runtime.getRuntime().exec(cmd);
			
			System.out.println("Schedule Backup Created: "+ backup);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}

		int interval = BackupIntervalService.getInterval();
		String time = BackupIntervalService.getBackupTime();
		BackupIntervalSchedular.schedulExecution(
				Interval.of(interval), 
				LocalTime.parse(time));
	}

}
