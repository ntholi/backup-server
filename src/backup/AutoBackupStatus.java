package backup;

public class AutoBackupStatus {

	private boolean autoBackupOn;
	private int interval;
	private String backupTime;
	
	public boolean isAutoBackupOn() {
		return autoBackupOn;
	}
	public void setAutoBackupOn(boolean autoBackupOn) {
		this.autoBackupOn = autoBackupOn;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	public String getBackupTime() {
		return backupTime;
	}
	public void setBackupTime(String backupTime) {
		this.backupTime = backupTime;
	}
}
