package backup;

public enum Interval {

	DAILY,
	WEEKLY,
	BI_WEEKLY,
	MONTHLY;

	public static Interval of(int index) {
		return values()[index];
	}
}
