package backup;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.prefs.Preferences;

public class BackupIntervalSchedular {

	private static ScheduledExecutorService executor;

	public static void schedulExecution(Interval interval, LocalTime time){
		if(executor == null){
			executor = Executors.newSingleThreadScheduledExecutor();
		}
		else{
			executor.shutdown();
			executor = Executors.newSingleThreadScheduledExecutor();
		}

		LocalDateTime today = LocalDateTime.now();
		LocalDateTime workTime = calculateNextWork(interval, time);
		long delay = ChronoUnit.SECONDS.between(today, workTime) + 1;
//		scheduler.scheduleAtFixedRate(new BackupJob(), delay, interval.getHours(), TimeUnit.HOURS);
		executor.schedule(new BackupJob(), delay, TimeUnit.SECONDS);
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		System.out.println("Backup Scheduler Set, Next backup at "
				+ formatter.format(today.plusSeconds(delay)) + " delay of "+ delay +" seconds");
	}
	
	private static LocalDateTime calculateNextWork(Interval interval, LocalTime time) {
		LocalDateTime today = LocalDateTime.of(LocalDate.now(), time);
		LocalDateTime date;
		if(interval == Interval.MONTHLY){
			date = today.with(TemporalAdjusters.lastDayOfMonth());
		}
		else if(interval == Interval.BI_WEEKLY){
			TemporalAdjuster sat = TemporalAdjusters.next(DayOfWeek.SATURDAY);
			date = today.with(sat).plusWeeks(1);
		}
		else if(interval == Interval.WEEKLY){
			TemporalAdjuster sat = TemporalAdjusters.next(DayOfWeek.SATURDAY);
			date = today.with(sat);
		}
		else{
			date = today.plusDays(1);
		}
		
		return date;
	}
	
	public static void shutdownNow(){
		if(executor != null){
			executor.shutdownNow();
		}
		System.out.println("Backup Scheduler Shutdown");
	}
	
	public static boolean isRunning(){
		return executor != null && !executor.isShutdown();
	}
}
